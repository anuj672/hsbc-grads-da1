public class currentAccount{
    static private long accountNumber=10000;
    private double accountBalance;
    private String gstNumber;
    private String customerName;
    private String businessName;
    private String street;
    private String city;
    private int zipCode;
    public currentAccount(String gstNumber,String customerName,String businessName){
        this.accountNumber=++accountNumber;
        this.customerName=customerName;
        this.businessName=businessName;

    }
    public currentAccount(String gstNumber,String customerName,String businessName,double accountBalance,String city,String street,int zipCode){
        this.accountBalance=accountBalance;
        this.accountNumber=++accountNumber;
        this.customerName=customerName;
        this.businessName=businessName;
        this.accountBalance=accountBalance;
        this.zipCode=zipCode;
        this.city=city;
        this.street=street;
            }
    public currentAccount(String gstNumber,String customerName,String businessName,double accountBalance){
        this.accountBalance=accountBalance;
        this.accountNumber=++accountNumber;
        this.customerName=customerName;
        this.businessName=businessName;
        this.accountBalance=accountBalance;
    }
    public double withdraw(double amount){
        if(amount-50000>0){
            this.accountBalance=this.accountBalance-amount;
            return this.accountBalance;
        }
        return 0;
    }
    public double deposit(double amount){
        this.accountBalance=this.accountBalance+amount;
        return this.accountBalance;
    }
    public void updateCommunication(String city,String street,int zipCode){
        this.city=city;
        this.street=street;
        this.zipCode=zipCode;
    }
    public static void main(String args[]){
        currentAccount ABC = new currentAccount("12A","Ashish","ABCfarm");
        ABC.deposit(60000);
        ABC.withdraw(5000);
    }

}