public class callByValueAndRefer{
    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,4};
        int operand1 = 5;
        int operand2 = 10;
        System.out.println("Values before call "+operand1+" "+ operand2);
        callbyValue(operand1,operand2);
        System.out.println("AFTER CALL"+ operand1 + " "+operand2);
        for(int i=0;i<arr.length;i++){
            System.out.println("Before call "+arr[i]);
        }
        callByref(arr);
        for(int i=0;i<arr.length;i++){
            System.out.println("After call "+arr[i]);
        }
    }
    public static void callbyValue(int operand1,int operand2){
        operand1=operand1*22;
        operand2=operand2*22;
        System.out.println("IN METHOD "+operand1+" "+operand2);
    }
    public static void callByref(int arr[]){
        for(int i=0;i<arr.length;i++){
            arr[i]=arr[i]+1;
            System.out.println("during call "+arr[i]);   
        }
    }
}