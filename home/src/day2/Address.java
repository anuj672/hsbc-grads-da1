package day2;

class Address {
	private String city;
	private String street;
	private int ZPcode;
	public Address(String city,String street,int ZPcode) {
		this.city=city;
		this.street=street;
		this.ZPcode=ZPcode;
		// TODO Auto-generated constructor stub
	}
	public void displayAddress(Address address){
		System.out.println("CITY "+ address.city);
		System.out.println("STREET "+ address.street);
		System.out.println("code  "+ address.ZPcode);
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getZPcode() {
		return ZPcode;
	}
	public void setZPcode(int zPcode) {
		ZPcode = zPcode;
	}

}
