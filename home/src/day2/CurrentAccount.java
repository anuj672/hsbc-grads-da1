package day2;

public class CurrentAccount {
	private static int accountNumber = 1000;
	private double accountBalance;
	private String GSTnumber;
	private String customerName;
	private String BusinessName;
	private String City;
	private String Street;
	private int ZPcode;
	private int MIN_ACCOUNT_BALANCE = 50000;
	private Address address;
	public CurrentAccount(double accountBalance,String GSTnumber,String customerName, String BusinessName) {
		this.accountBalance = accountBalance;
		this.GSTnumber=GSTnumber;
		this.customerName=customerName;
		this.BusinessName=BusinessName;
		this.accountNumber = ++ accountNumber;
		// TODO Auto-generated constructor stub
	}
	public CurrentAccount(String GSTnumber,String customerName, String BusinessName) {
		this.GSTnumber=GSTnumber;
		this.customerName=customerName;
		this.BusinessName=BusinessName;
		this.accountNumber = ++ accountNumber;
		// TODO Auto-generated constructor stub
	}
	public CurrentAccount(double accountBalance,String GSTnumber,String customerName, String BusinessName,Address address){
		this.GSTnumber=GSTnumber;
		this.accountBalance=accountBalance;
		this.customerName=customerName;
		this.BusinessName=BusinessName;
		this.address = address;
		this.accountNumber = ++ accountNumber;
		// TODO Auto-generated constructor stub
	}
	public boolean validateMinBalance(double accountBalance,double amount){
		return ((accountBalance-amount)>=MIN_ACCOUNT_BALANCE);
	}
	public double withdraw(double amount){
		if(validateMinBalance(this.accountBalance,amount)){
			this.accountBalance = this.accountBalance - amount;
			return this.accountBalance;
		}
		return 0;
	}
	public double deposit(double amount){
		this.accountBalance = this.accountBalance + amount;
		return this.accountBalance;
	}
	public double checkBalance(){
		return this.accountBalance;
	}
	public void displayDetails(){
		System.out.println("GSTNumber is "+ this.GSTnumber);
		System.out.println("Name is "+ this.customerName);
		System.out.println("Business Name is "+ this.BusinessName);
		System.out.println("Account Balance is "+ this.accountBalance);
		if(this.address == null){
			System.out.println("ADDRESS DETAILS NOT GIVEN");
		}
		else{
			this.address.displayAddress(address);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CurrentAccount Rajesh = new CurrentAccount(100000,"12A","Rajesh","HSBC");
		Address address_sunil = new Address("Pune","East street",411001);
		CurrentAccount sunil = new CurrentAccount(100000,"17B","Sunil","Capgemini",address_sunil);
		System.out.println(Rajesh.checkBalance());
		System.out.println(Rajesh.withdraw(1000));
		System.out.println(Rajesh.checkBalance());
		Rajesh.displayDetails();
		sunil.displayDetails();
	}

}
