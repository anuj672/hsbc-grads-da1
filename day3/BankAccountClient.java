abstract class BankAccount{
    protected String name;
    protected String GstNo;
    protected static int accountNo=1000;
    protected double accountBalance;
    public final void deposit(double amount){
        this.accountBalance += amount;
    }
    public final String getCustomerDetails() {
        return "Customer name: " + this.name + " Balance: " + this.accountBalance + "A/C No: " + this.accountNo;
    }

    public final double getAccountBalance(){
        return this.accountBalance;
    }
    abstract public void withdrawl(double amount);
    abstract public void loanAmount(double loan);
    abstract public double loanEligibility();
    public BankAccount(String name,String GstNo,double accountBalance){
        this.name=name;
        this.GstNo=GstNo;
        this.accountBalance=accountBalance;
        this.accountNo =++ accountNo;
    }

}
final class CurrentAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 25_00000;
    private final long MIN_BALANCE = 25_000;
    public CurrentAccount(String name,String GstNo,double accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if((super.accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
        }
    }
    final public void loanAmount(double loan){
        if(loan <=this.loanEligibility()){
            System.out.println("granted");
        }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }

}


final class SavingAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 5_00000;
    private final long MIN_BALANCE = 10_000;
    private final long WITHDRAWL_LIMIT = 10_000; 
    public SavingAccount(String name,String GstNo,double accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if(amount <= WITHDRAWL_LIMIT && (super.accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
            
        }
    }
    final public void loanAmount(double loan){
            if(loan <= this.loanEligibility()){
                System.out.println("granted");
            }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }
} 
final class SalariedAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 10_00000;
    private final long WITHDRAWL_LIMIT = 15_000; 
    private final long MIN_BALANCE = 0;
    public SalariedAccount(String name,String GstNo,int accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if(amount < WITHDRAWL_LIMIT && (accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
            
        }
    }
    final public void loanAmount(double loan){
            if(loan <= this.loanEligibility()){
                System.out.println("granted");
            }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }
}   
public class BankAccountClient{
    public static void main(String []args){
        BankAccount account = null;
        int choice = Integer.parseInt(args[0]);
        switch(choice){
            case 1:account =  new CurrentAccount("Anuj","12A",1000000);
            break;
            case 2:account = new SavingAccount("Yash","32c",12000);
            break;
            case 3:account = new SalariedAccount("Sahil","4512A",150000);
            break;
            default :account =  new CurrentAccount("Anuj","12A",10000); 
        }
    String details = account.getCustomerDetails();
    System.out.println(details);
    account.withdrawl(500);
    details = account.getCustomerDetails();
    System.out.println(details);
    }
}

