interface Insurance{
    double calculatePremium(double insuredAmount,String car,String model);
    int payPremium(double premiumAmount,String vehicleNumber,String model);
}
class BAJAJInsurance implements Insurance{
    static int policyNumberBAJAJ=1000;
    public double calculatePremium(double insuredAmount,String car,String model){
        double premiumAmount;
        premiumAmount = (insuredAmount*1.15) - insuredAmount;
        return premiumAmount;
    }
    public int payPremium(double premiumAmount,String vehicleNumber,String model){
        return ++policyNumberBAJAJ;
    }
} 
class TATAAIG implements Insurance{
     
    static int policyNumberTATA=2000;
    public double calculatePremium(double insuredAmount,String car,String model){
        double premiumAmount;
        premiumAmount = (insuredAmount*1.20) - insuredAmount;
        return premiumAmount;
    }
    public int payPremium(double premiumAmount,String vehicleNumber,String model){
        return ++policyNumberTATA;
    }
    
} 
abstract class BankAccount{
    protected String name;
    protected String GstNo;
    protected static int accountNo=1000;
    protected double accountBalance;
    public final boolean InsuranceGrant(double premiumAmount){
        if(premiumAmount<=accountBalance){
            return true;
        }
        return false;
    }
    public final void deposit(double amount){
        this.accountBalance += amount;
    }
    public final String getCustomerDetails() {
        return "Customer name: " + this.name + " Balance: " + this.accountBalance + "A/C No: " + this.accountNo;
    }

    public final double getAccountBalance(){
        return this.accountBalance;
    }
    abstract public void withdrawl(double amount);
    abstract public void loanAmount(double loan);
    abstract public double loanEligibility();
    public BankAccount(String name,String GstNo,double accountBalance){
        this.name=name;
        this.GstNo=GstNo;
        this.accountBalance=accountBalance;
        this.accountNo =++ accountNo;
    }

}
final class CurrentAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 25_00000;
    private final long MIN_BALANCE = 25_000;
    public CurrentAccount(String name,String GstNo,double accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if((super.accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
        }
    }
    final public void loanAmount(double loan){
        if(loan <=this.loanEligibility()){
            System.out.println("granted");
        }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }

}


final class SavingAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 5_00000;
    private final long MIN_BALANCE = 10_000;
    private final long WITHDRAWL_LIMIT = 10_000; 
    public SavingAccount(String name,String GstNo,double accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if(amount <= WITHDRAWL_LIMIT && (super.accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
            
        }
    }
    final public void loanAmount(double loan){
            if(loan <= this.loanEligibility()){
                System.out.println("granted");
            }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }
} 
final class SalariedAccount extends BankAccount{
    private final long LOAN_ELIGIBILITY = 10_00000;
    private final long WITHDRAWL_LIMIT = 15_000; 
    private final long MIN_BALANCE = 0;
    public SalariedAccount(String name,String GstNo,int accountBalance){
        super(name,GstNo,accountBalance);
    }
    final public void withdrawl(double amount){
        if(amount < WITHDRAWL_LIMIT && (accountBalance-amount)>=MIN_BALANCE){
            super.accountBalance=super.accountBalance-amount;
            
        }
    }
    final public void loanAmount(double loan){
            if(loan <= this.loanEligibility()){
                System.out.println("granted");
            }
    }
    final public double loanEligibility(){
        return LOAN_ELIGIBILITY;
    }
}   
public class BankAccountInsurance{
    public static void main(String[] args) {
         BankAccount account = null;
        int choiceAccountType = Integer.parseInt(args[0]);
        switch(choiceAccountType){
            case 1:account =  new CurrentAccount("Anuj","12A",100);
            break;
            case 2:account = new SavingAccount("Yash","32c",12000);
            break;
            case 3:account = new SalariedAccount("Sahil","4512A",150000);
            break;
            default :account =  new CurrentAccount("Anuj","12A",10000); 
        }
        Insurance insurance =  null;
        int choiceInsurace = Integer.parseInt(args[1]);
        if (choiceInsurace == 1) {
            BAJAJInsurance bajajInsurance = new BAJAJInsurance();
            insurance = bajajInsurance;
    }
    else if (choiceInsurace == 2) {
        TATAAIG tataAIG = new TATAAIG();
        insurance = tataAIG;
    }
    double premiumAmount = insurance.calculatePremium(45000,"zen","2015");
    if(account.InsuranceGrant(premiumAmount)){
    int policyNumber= insurance.payPremium(premiumAmount,"MH12 AB 1234","2015");
    System.out.println(policyNumber);
    }
    else{
        System.out.println("INSUFFICIENT BALANCE TO GRANT INSURANCE");
    }
}
}