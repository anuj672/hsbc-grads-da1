interface Insurance{
    double calculatePremium(double insuredAmount,String car,String model);
    int payPremium(double premiumAmount,String vehicleNumber,String model);
}
class BAJAJInsurance implements Insurance{
    static int policyNumberBAJAJ=1000;
    public double calculatePremium(double insuredAmount,String car,String model){
        double premiumAmount;
        premiumAmount = (insuredAmount*1.15) - insuredAmount;
        return premiumAmount;
    }
    public int payPremium(double premiumAmount,String vehicleNumber,String model){
        return ++policyNumberBAJAJ;
    }
} 
class TATAAIG implements Insurance{
     
    static int policyNumberTATA=2000;
    public double calculatePremium(double insuredAmount,String car,String model){
        double premiumAmount;
        premiumAmount = (insuredAmount*1.20) - insuredAmount;
        return premiumAmount;
    }
    public int payPremium(double premiumAmount,String vehicleNumber,String model){
        return ++policyNumberTATA;
    }
    
} 
public class InsurancePolicy{
    public static void main(String[] args) {
        Insurance insurance =  null;
        int choice = Integer.parseInt(args[0]);
        if (choice == 1) {
            BAJAJInsurance bajajInsurance = new BAJAJInsurance();
            insurance = bajajInsurance;
    }
    else if (choice == 2) {
        TATAAIG tataAIG = new TATAAIG();
        insurance = tataAIG;
    }
    double premiumAmount = insurance.calculatePremium(45000,"zen","2015");
    int policyNumber= insurance.payPremium(premiumAmount,"MH12 AB 1234","2015");
    System.out.println(policyNumber);
}
}