package com.hsbc.da1.service;

import java.util.Collection;

import com.hsbc.da1.dao.EmployeeManagementDAO;
import com.hsbc.da1.dao.SetBasedEmployeeManagementDAO;
import com.hsbc.da1.model.EmployeeManagement;
import com.hsbc.da1.exception.*;
public class EmployeeManagementServiceImpli implements EmployeeManagementService {
	
	
	private EmployeeManagementDAO dao; 
	public  EmployeeManagementServiceImpli(EmployeeManagementDAO dao){
		this.dao=dao;
	}
	@Override
	public EmployeeManagement addEmployee(String employeeName,double employeeSalary, int employeeAge) {
		EmployeeManagement employeeManagement = new EmployeeManagement(employeeName,employeeSalary,employeeAge);
		EmployeeManagement employeeAdded = this.dao.addEmployee(employeeManagement);
		return employeeAdded;
	}

	@Override
	public void deleteEmployee(int employeeID) {
		this.dao.deleteEmployee(employeeID);
		
	}

	@Override
	public EmployeeManagement updateEmployee(String employeeName,double employeeSalary, int employeeAge,int employeeID) {
		EmployeeManagement employeeManagement = new EmployeeManagement(employeeName,employeeSalary,employeeAge);
		EmployeeManagement employeeUpdated = this.dao.updateEmployee(employeeID,employeeManagement);
		return employeeUpdated;
	}

	@Override
	public EmployeeManagement applyLeaves(String employeeName, double employeeSalary,int employeeAge, int noOfLeaves)throws InsufficientLeavesAvailable {
		EmployeeManagement employeeManagementCheck = this.dao.fetchEmployeeByName(employeeName);
		if(noOfLeaves >= 10 && employeeManagementCheck.getNO_OF_LEAVES() - noOfLeaves <0){
			throw new InsufficientLeavesAvailable("INSUFFICIENT LEAVES");
		}else{
			int employeeID = employeeManagementCheck.getEmployeeID();
			EmployeeManagement employeeManagement = this.dao.updateLeaves(employeeID, noOfLeaves);
			return employeeManagement;
		}
		// TODO Auto-generated method stub
	}

	@Override
	public EmployeeManagement fetchEmployeeByID(int employeeID) {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeeByID(employeeID);
	}

	@Override
	public EmployeeManagement fetchEmployeeByName(String employeeName) {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeeByName(employeeName);
	}

	@Override
	public Collection<EmployeeManagement> listEmployee() {
		// TODO Auto-generated method stub
		return this.dao.listEmployee();
	}

	

}
