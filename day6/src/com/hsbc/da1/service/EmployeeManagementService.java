package com.hsbc.da1.service;
import com.hsbc.da1.exception.*;
import java.util.Collection;

import com.hsbc.da1.model.EmployeeManagement;

public interface EmployeeManagementService {

	EmployeeManagement addEmployee(String employeeName,double employeeSalary,int employeeAge);
	void deleteEmployee(int employeeID);
	EmployeeManagement fetchEmployeeByID(int employeeID);
	EmployeeManagement fetchEmployeeByName(String employeeName);
	Collection<EmployeeManagement> listEmployee();
	EmployeeManagement updateEmployee(String employeeName,double employeeSalary, int employeeAge, int employeeID);
	EmployeeManagement applyLeaves(String employeeName, double employeeSalary,int employeeAge, int noOfLeaves)throws InsufficientLeavesAvailable;
}
