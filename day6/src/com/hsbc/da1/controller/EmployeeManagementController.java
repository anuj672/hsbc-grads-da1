package com.hsbc.da1.controller;

import java.util.Collection;
import java.util.Set;

import com.hsbc.da1.exception.InsufficientLeavesAvailable;
import com.hsbc.da1.model.EmployeeManagement;
import com.hsbc.da1.service.EmployeeManagementService;
import com.hsbc.da1.service.EmployeeManagementServiceImpli;

public class EmployeeManagementController {
	private EmployeeManagementService  service;
	public EmployeeManagementController(EmployeeManagementService service){
		this.service = service;
	}
	
	
public EmployeeManagement addEmployee(String employeeName,double employeeSalary, int employeeAge){
		EmployeeManagement employeeManagement = this.service.addEmployee(employeeName, employeeSalary, employeeAge);
		return employeeManagement;
	}
	void deleteEmployee(int employeeID){
		this.service.deleteEmployee(employeeID);
	}
	public	EmployeeManagement fetchEmployeeByID(int employeeID){
		EmployeeManagement employeeManagement = this.service.fetchEmployeeByID(employeeID);
		return employeeManagement;
	}
	public EmployeeManagement fetchEmployeeByName(String employeeName){
		EmployeeManagement employeeManagement = this.service.fetchEmployeeByName(employeeName);
		return employeeManagement;
	}
	public Collection<EmployeeManagement> listEmployee(){
		Collection<EmployeeManagement> employeesManagement = this.service.listEmployee();
		return employeesManagement;
	}
	public EmployeeManagement updateEmployee(String employeeName,double employeeSalary, int employeeAge, int employeeID){
		EmployeeManagement employeeManagement = this.service.updateEmployee(employeeName, employeeSalary, employeeAge, employeeID);
		return employeeManagement;
	}
	public EmployeeManagement applyLeaves(String employeeName, double employeeSalary,int employeeAge, int noOfLeaves)throws InsufficientLeavesAvailable{
	EmployeeManagement employeeManagement = this.service.applyLeaves(employeeName, employeeSalary, employeeAge, noOfLeaves);
	return employeeManagement;
}
}
