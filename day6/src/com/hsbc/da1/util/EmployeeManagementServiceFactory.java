package com.hsbc.da1.util;

import com.hsbc.da1.dao.EmployeeManagementDAO;
import com.hsbc.da1.service.*;

public class EmployeeManagementServiceFactory {

	private EmployeeManagementServiceFactory() {// TODO Auto-generated constructor stub
	}
	public static EmployeeManagementService getEmployeeManagementService(EmployeeManagementDAO dao){
		return new EmployeeManagementServiceImpli(dao);
	}
}
