package com.hsbc.da1.util;

import com.hsbc.da1.dao.*;

public class EmployeeManagementDAOFactory {

	private EmployeeManagementDAOFactory() {// TODO Auto-generated constructor stub
	}
	public static EmployeeManagementDAO getEmployeeManagementDAO(){
		return new SetBasedEmployeeManagementDAO();
	}
}
