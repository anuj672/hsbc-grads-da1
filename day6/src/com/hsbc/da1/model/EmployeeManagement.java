package com.hsbc.da1.model;

public class EmployeeManagement {
	
	private String employeeName;
	private int employeeID;
	private int employeeAge;
	private double employeeSalary;
	private int NO_OF_LEAVES = 40;
	private static int counter =1000;

	public EmployeeManagement(String employeeName,double employeeSalary,int employeeAge) {
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.employeeAge=employeeAge;
		this.employeeID = counter++;
	}
		
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public double getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public int getNO_OF_LEAVES() {
		return NO_OF_LEAVES;
	}

	public void setNO_OF_LEAVES(int nO_OF_LEAVES) {
		NO_OF_LEAVES = nO_OF_LEAVES;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + employeeID;
		result = prime * result
				+ ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EmployeeManagement)) {
			return false;
		}
		EmployeeManagement other = (EmployeeManagement) obj;
		if (employeeID != other.employeeID) {
			return false;
		}
		if (employeeName == null) {
			if (other.employeeName != null) {
				return false;
			}
		} else if (!employeeName.equals(other.employeeName)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "EmployeeManagement [employeeName=" + employeeName
				+ ", employeeID=" + employeeID + ", employeeAge=" + employeeAge
				+ ", employeeSalary=" + employeeSalary + ", NO_OF_LEAVES="
				+ NO_OF_LEAVES + "]";
	}
}
