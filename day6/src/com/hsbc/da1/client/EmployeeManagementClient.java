package com.hsbc.da1.client;

import com.hsbc.da1.controller.EmployeeManagementController;
import com.hsbc.da1.dao.EmployeeManagementDAO;
import com.hsbc.da1.exception.InsufficientLeavesAvailable;
import com.hsbc.da1.model.EmployeeManagement;
import com.hsbc.da1.service.EmployeeManagementService;
import com.hsbc.da1.util.EmployeeManagementDAOFactory;
import com.hsbc.da1.util.EmployeeManagementServiceFactory;

public class EmployeeManagementClient  {

	public static void main(String args[])throws InsufficientLeavesAvailable {
		EmployeeManagementDAO dao = EmployeeManagementDAOFactory.getEmployeeManagementDAO();
		EmployeeManagementService service = EmployeeManagementServiceFactory.getEmployeeManagementService(dao);
		EmployeeManagementController controller = new EmployeeManagementController(service);
		EmployeeManagement Anuj = controller.addEmployee("Anuj",56777, 21);
		EmployeeManagement Yash = controller.addEmployee("Yash",56777, 26);
		System.out.println("id "+ Anuj.getEmployeeID());
		System.out.println("id "+ Yash.getEmployeeID());
		try{;
		Anuj = controller.applyLeaves("Anuj",56777, 21, 5);
		}catch(InsufficientLeavesAvailable exception){
			System.out.print("insufficient leaves available");
			
		}
		System.out.println("id "+ Anuj.getNO_OF_LEAVES());
		Anuj= controller.updateEmployee("sam", 5688, 21, 1000);
		
	}
}
