package com.hsbc.da1.exception;

public class InsufficientLeavesAvailable extends Exception {

	public InsufficientLeavesAvailable(String Message) {
			super(Message);
	}
	public String getMessage() {
		return super.getMessage();
	}


}
