package com.hsbc.da1.dao;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.hsbc.da1.model.EmployeeManagement;

public  class SetBasedEmployeeManagementDAO implements EmployeeManagementDAO {
	
	private  Set<EmployeeManagement> employeesManagement =  new HashSet<>();

	@Override
	public EmployeeManagement addEmployee(EmployeeManagement employeeManagement) {
		this.employeesManagement.add(employeeManagement);
		return employeeManagement;
	}

	@Override
	public EmployeeManagement updateEmployee(int employeeID,EmployeeManagement employeManagement) {
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			if(employeeManagement.getEmployeeID()==employeeID){
				this.employeesManagement.remove(employeeManagement);
				employeeManagement = employeManagement;
				this.employeesManagement.add(employeeManagement);
				return employeeManagement;
			}
		}
		return null;
}

	@Override
	public void deleteEmployee(int employeeID) {
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			if(employeeManagement.getEmployeeID()==employeeID){
				this.employeesManagement.remove(employeeManagement);
			}
	}
}
	@Override
	public EmployeeManagement fetchEmployeeByID(int employeeID) {
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			if(employeeManagement.getEmployeeID()==employeeID){
				return employeeManagement;		
			}
		}
		return null;
}
	
	@Override
	public EmployeeManagement fetchEmployeeByName(String employeeName) {
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			if(employeeManagement.getEmployeeName()==employeeName){
				return employeeManagement;		
			}
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<EmployeeManagement> listEmployee() {
		return employeesManagement;
		}

	@Override
	public int displayLeaves(int employeeID) {
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			if(employeeManagement.getEmployeeID()==employeeID){
				return employeeManagement.getNO_OF_LEAVES();
			}
		}
	return 0;
	}
	public EmployeeManagement updateLeaves(int employeeID,int remainingLeaves){
		
		Iterator<EmployeeManagement> it = employeesManagement.iterator();
		while(it.hasNext()){
			EmployeeManagement employeeManagement = it.next();
			System.out.println(employeeManagement);
			if(employeeManagement.getEmployeeID()==employeeID){
				System.out.println(employeeID);
				employeeManagement.setNO_OF_LEAVES(40-remainingLeaves);
				
				 return employeeManagement;
			}
		}
		return null ;
	}
}
	
	
