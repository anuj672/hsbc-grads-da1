package com.hsbc.da1.dao;

import java.util.Collection;

import com.hsbc.da1.model.EmployeeManagement;

public interface EmployeeManagementDAO {
	EmployeeManagement addEmployee(EmployeeManagement employeeManagement);
	void deleteEmployee(int employeeID);
	EmployeeManagement fetchEmployeeByID(int employeeID);
	EmployeeManagement fetchEmployeeByName(String employeeName);
	Collection<EmployeeManagement> listEmployee();
	EmployeeManagement updateEmployee(int employeeID,EmployeeManagement employeManagement);
	int displayLeaves(int employeeID);
	public EmployeeManagement updateLeaves(int employeeID,int remainingLeaves);
}
