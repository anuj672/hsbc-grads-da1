package util;
import com.hsbc.da1.DAO.*;
public class SavingsAccountDAOFactory {
		public static SavingsAccountDAO getSavingsAccountDAO(int value){
			if (value==1){
				System.out.println("Array list");
			return new ArrayListBackedSavingsAccountImpli();
			
			}
			else if (value ==2){
			System.out.println("linked list");
			return new LinkedListBackedSavingsAccountImpli();
			
			}
			else
			{
				return new ArrayBackedSavingsAccountImpli();
			}

}
}
