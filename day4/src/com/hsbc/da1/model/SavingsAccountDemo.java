package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {

	public static void main(String args[]){
		SavingsAccount sa = new SavingsAccount("dinesh",35000); 
		SavingsAccount dinesh = new SavingsAccount("dinesh",3500);
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		savingsAccountSet.add(sa);
		savingsAccountSet.add(dinesh);
		
		Iterator <SavingsAccount> it = savingsAccountSet.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
	}
}
