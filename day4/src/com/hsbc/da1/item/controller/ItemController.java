package com.hsbc.da1.item.controller;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.item.model.*;
import com.hsbc.da1.item.service.ItemService;
import com.hsbc.da1.item.service.ItemServiceImpli;
public class ItemController {
	private ItemService itemService = new ItemServiceImpli();
	public Item addItem(String itemName, String itemType){
		Item item = this.itemService.saveItem(itemName, itemType);
		return item;
	}
	public void deleteItem(int itemNumber){
		this.itemService.deleteItem(itemNumber);
	}
	public Item[] listItem(){
		Item items[]= this.itemService.listItem();
		return items;
	}
	public Item fetchItemByID(int itemNumber) throws CustomerNotFound{
		Item item = this.itemService.findItemById(itemNumber); 
		return item;
	}
}
