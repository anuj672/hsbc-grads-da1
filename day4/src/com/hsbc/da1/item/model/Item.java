package com.hsbc.da1.item.model;

public class Item {
	private String itemName;
	private String itemType;
	private int itemNumber;
	private static int counter=1000;
	
	public Item(String itemName, String itemType) {
		this.itemName=itemName;
		this.itemType = itemType;
		this.itemNumber=counter++;	
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public int getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}

}
