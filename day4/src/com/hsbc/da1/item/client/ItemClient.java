package com.hsbc.da1.item.client;

import com.hsbc.da1.item.controller.ItemController;
import com.hsbc.da1.item.model.*;

public class ItemClient {

	public static void main(String args[]){
		ItemController itemController = new ItemController();
		Item chocolate = itemController.addItem("dairymilk", "chocolate");
		System.out.println(chocolate.getItemNumber());
	}

}
