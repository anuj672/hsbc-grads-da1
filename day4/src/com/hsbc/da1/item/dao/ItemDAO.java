package com.hsbc.da1.item.dao;
import com.hsbc.da1.item.model.*;
public interface ItemDAO {

   Item saveItem(Item item);
   
   Item[] listItem();
   
   void deleteItem(int itemNumber);
   
   public Item updateItem(Item item);
   
   Item findItemById(int itemNumber);
   
   

}
