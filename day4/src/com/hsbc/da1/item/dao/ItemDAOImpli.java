package com.hsbc.da1.item.dao;
import com.hsbc.da1.item.model.*;
public class ItemDAOImpli implements ItemDAO{
	
	private static Item items[] =  new Item[100];
	private static int counter = 0;
	public Item saveItem(Item item) {
		items[counter++]= item;
		return item;
	}

	public Item[] listItem() {
		return items;
	}

	public void deleteItem(int itemNumber) {
		for(int index=0;index<items.length;index++){
			if(items[index].getItemNumber()==itemNumber){
				items[index]=null;
				break;
			}
		}
		
	}
	public Item findItemById(int itemNumber) {
		for(int index=0;index<items.length;index++){
			if(items[index].getItemNumber()==itemNumber){
				return items[index];
			}
		}
		return null;
	}
	public Item updateItem(Item item){
		for(int index=0;index<items.length;index++){
			if(items[index].getItemNumber()==item.getItemNumber()){
				items[index]=item;
				break;
			}
		}
		return item;
	}
	
}
