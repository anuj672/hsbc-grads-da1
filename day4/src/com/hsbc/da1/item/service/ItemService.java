package com.hsbc.da1.item.service;

import com.hsbc.da1.item.model.*;

public interface ItemService {
	
	Item saveItem(String itemName,String itemType);
	void deleteItem(int itemNumber);
	void updateItem(Item item);
	Item[] listItem();
	Item findItemById(int itemNumber);

}
