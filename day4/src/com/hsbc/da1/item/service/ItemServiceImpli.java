package com.hsbc.da1.item.service;
import com.hsbc.da1.item.model.*;
import com.hsbc.da1.item.dao.*;
public class ItemServiceImpli implements ItemService {
 
	ItemDAO dao = new ItemDAOImpli();

	public Item saveItem(String itemName, String itemType) {
		Item item = new Item(itemName,itemType);
		Item itemCreated = this.dao.saveItem(item);
		return itemCreated;
	}

	public void deleteItem(int itemNumber) {
		this.dao.deleteItem(itemNumber);
		// TODO Auto-generated method stub
		
	}

	public void updateItem(Item item) {
		this.dao.updateItem(item);
		// TODO Auto-generated method stub
		
	}

	public Item[] listItem() {
		// TODO Auto-generated method stub
		Item items[] = this.dao.listItem();
		return items;
	}

	public Item findItemById(int itemNumber) {
		Item item = this.dao.findItemById(itemNumber);
		return item;
	}

	
}
