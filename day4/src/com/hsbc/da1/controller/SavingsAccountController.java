package com.hsbc.da1.controller;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.*;
public class SavingsAccountController {
	private SavingsAccountService savingsAccountService = new SavingsAccountServiceImpli();
	public SavingsAccount openSavingsAccount(String customerName, double accountBalance){
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance);
		return savingsAccount;
	}
	public void deleteSavingsAccount(long accountNumber){
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}
	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> accounts = this.savingsAccountService.fetchSavingsAccount();
		return accounts;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)throws CustomerNotFound {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByID(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount)throws CustomerNotFound {
		return this.savingsAccountService.withdraw(accountId, amount);
	}
	public double deposit(long accountId, double amount)throws CustomerNotFound {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) throws CustomerNotFound{
		return this.savingsAccountService.checkBalance(accountId);
	}



}
