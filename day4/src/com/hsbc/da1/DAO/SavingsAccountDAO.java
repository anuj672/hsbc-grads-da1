package com.hsbc.da1.DAO;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;
public interface SavingsAccountDAO {
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount);
	
	List<SavingsAccount> fetchSavingsAccount();
	
	SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFound;
	

}
