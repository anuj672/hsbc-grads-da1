package com.hsbc.da1.DAO;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;

import java.util.*;
public class LinkedListBackedSavingsAccountImpli implements SavingsAccountDAO {

	private List<SavingsAccount> savingsAccountsLinkedList = new LinkedList<>();
	//private static SavingsAccount savingsAccounts[]=new SavingsAccount[100];
	//private static int counter;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount){
		this.savingsAccountsLinkedList.add(savingsAccount);
		//savingsAccounts[counter++]=savingsAccount;
		return savingsAccount;
	}
	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount){
		for(SavingsAccount sa: savingsAccountsLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;

		}
	public void deleteSavingsAccount(long accountNumber){
		for(SavingsAccount sa: savingsAccountsLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountsLinkedList.remove(sa);
			}
		}

	}
	public List<SavingsAccount> fetchSavingsAccount(){
		return this.savingsAccountsLinkedList;
	}
	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFound{
		for(SavingsAccount sa: savingsAccountsLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
	
		throw new CustomerNotFound("Customer not found"); 
	}
}
