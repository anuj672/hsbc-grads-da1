package com.hsbc.da1.DAO;
import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;
public class ArrayBackedSavingsAccountImpli implements SavingsAccountDAO {

	private static SavingsAccount savingsAccounts[]=new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount){
		savingsAccounts[counter++]=savingsAccount;
		return savingsAccount;
	}
	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount){
		for(int i =0;i<savingsAccounts.length;i++){
			if(savingsAccounts[i].getAccountNumber() == accountNumber){
				savingsAccounts[i]= savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	public void deleteSavingsAccount(long accountNumber){
		for(int i=0;i<savingsAccounts.length;i++){
			if(savingsAccounts[i].getAccountNumber() == accountNumber){
				savingsAccounts[i]=null;
			}
		}
	}
	public List<SavingsAccount> fetchSavingsAccount(){
		return Arrays.asList(savingsAccounts);
	}
	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFound{
		for(int i=0;i<savingsAccounts.length;i++){
			if(savingsAccounts[i]!= null && savingsAccounts[i].getAccountNumber()== accountNumber){
				return savingsAccounts[i];
			}
		}
		throw new CustomerNotFound("Customer not found"); 
	}
}
