package com.hsbc.da1.DAO;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;

import java.util.*;
public class ArrayListBackedSavingsAccountImpli implements SavingsAccountDAO {

	private List<SavingsAccount> savingsAccountsList = new ArrayList<>();
	//private static SavingsAccount savingsAccounts[]=new SavingsAccount[100];
	//private static int counter;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount){
		this.savingsAccountsList.add(savingsAccount);
		//savingsAccounts[counter++]=savingsAccount;
		return savingsAccount;
	}
	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount){
		for(SavingsAccount sa: savingsAccountsList) {
			if (sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;

		}
	public void deleteSavingsAccount(long accountNumber){
		for(SavingsAccount sa: savingsAccountsList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountsList.remove(sa);
			}
		}

	}
	public List<SavingsAccount> fetchSavingsAccount(){
		return this.savingsAccountsList;
	}
	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFound{
		for(SavingsAccount sa: savingsAccountsList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
	
		throw new CustomerNotFound("Customer not found"); 
	}
}
