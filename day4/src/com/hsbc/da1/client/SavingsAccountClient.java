package com.hsbc.da1.client;

import com.hsbc.da1.model.*;
import com.hsbc.da1.controller.*;
import com.hsbc.da1.exception.CustomerNotFound;

public class SavingsAccountClient {
	
	public static void main(String args[]) throws CustomerNotFound{
		SavingsAccountController savingsAccountController =  new SavingsAccountController();
		
		SavingsAccount Anuj = savingsAccountController.openSavingsAccount("Anuj", 25000);
		SavingsAccount Yash = savingsAccountController.openSavingsAccount("Yash", 25000);
		
		System.out.println("id "+ Anuj.getAccountNumber());
		System.out.println("id "+ Yash.getAccountNumber());
		try{
			SavingsAccount YashC =savingsAccountController.fetchSavingsAccountByAccountId(12);
		}catch(CustomerNotFound exception){
			System.out.println(exception.getMessage());
		}
	}

}
