package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;
public interface SavingsAccountService {
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetchSavingsAccount();
	
	public SavingsAccount fetchSavingsAccountByID(long accountNumber)throws CustomerNotFound ;
	
	public double withdraw(long accountNumber,double amount)throws CustomerNotFound ;
	
	public double deposit(long accountNumber,double amount) throws CustomerNotFound;
	
	public double checkBalance(long accountNumber) throws CustomerNotFound;

}
