package com.hsbc.da1.service;
import java.util.List;

import com.hsbc.da1.DAO.SavingsAccountDAO;

import static util.SavingsAccountDAOFactory.*;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;
public class SavingsAccountServiceImpli implements SavingsAccountService {

	private SavingsAccountDAO dao = getSavingsAccountDAO(3);
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName,accountBalance);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;

	}

	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
		
	}

	public List<SavingsAccount> fetchSavingsAccount() {
		return  this.dao.fetchSavingsAccount();
	}

	public SavingsAccount fetchSavingsAccountByID(long accountNumber)throws CustomerNotFound {
	
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountNumber, double amount) throws CustomerNotFound  {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		if(savingsAccount != null){
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if(currentAccountBalance>=amount){
				currentAccountBalance = currentAccountBalance-amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountNumber, savingsAccount);
				return currentAccountBalance;
			}
		}
		return 0;
	}

	public double deposit(long accountNumber, double amount) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		if(savingsAccount != null){
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return currentAccountBalance;
		}
		return 0;
	}

	public double checkBalance(long accountNumber)throws CustomerNotFound {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
 
	}

}
